��    ;      �  O   �        5   	  n   ?  �   �  )   =     g  f   �     �  4        @     E     N     n  #   z  !   �     �  (   �     �     �            "   $     G     N     b     g     {     �     �     �  
   6	     A	  !   J	     l	     y	  (   	     �	  #   �	  %   �	     �	     
     '
     <
     P
     k
     �
     �
     �
     �
     �
     �
       
   #     .  
   <     G  	   [     e     y  �  �  �   l  �     (  �  �   �  I   �    �  W   �  e   +     �  %   �  7   �  '   �  =   &  +   d  #   �  :   �     �     �       V     N   n  
   �  8   �       D        U  i   h  /   �  �        �     �  M     #   [  
     E   �     �  H   �  M         m  (   �     �     �  $   �  *        C  .   `  &   �  &   �  )   �  ,        4     S     i  $   �  1   �     �  "   �  G                )   "      *                      .   3   
   '   (                  1       $          0   7         :   ;   %   #         8       4         2   +       /      ,              9       -   	                                                  !   5                     6   &       A completely revamped UI is the star of this release. A much better Spanish translation, a new icon, and many more improvements from the past 3 months are included. A tool to pick a random number or list item. Pick what chore to do, a number between 1 and 100, whether or not to jump on your mom's bed, etc. Adds a coin flip view and an improved UI. Adds a menu and some bugfixes. Adds a new Spanish translation, help docs, revamps keyboard shortcuts, and makes the UI more adaptive. Adds some nice random colors. Adds tons of new actions and keyboard shortcuts too. Coin Controls Convert from Number to Roulette Copy result Example: Layla/Rose/Cleveland/Lampy First Flathub release with GTK 4. Flip the coin Follows the GNOME HIG and uses libhandy. From: Generate Heads Make randomization easy New shortcuts, new docs, new life. Number Number to _Roulette Pick Pick, spin, or flip Random Random;Roulette;Coin;Number; Randomizing made easy. Removes the random colors for a noticable identity, and uses the latest GNOME 41 things, that came out on the same day as this. Repository Roulette Spin Roulette and delete selected Switch views Tails The first release, welcome to the world! To: Translated into German and Russian. Type a list, seperating values with / Version 0.1 "Beginning": Version 0.2 "Makeover": Version 0.3 "Penny": Version 0.4 "Flat": Version 0.5 "Caterpillar": Version 0.6 "Advancing": Version 0.7 "Circus": Version 0.8 "Sync": Version 0.9 "Global": Version 1.0 "Adapting": Version 1.1 "Fix-Behind": Version 1.2 "Flipped": Version 1.3 "Copier": Waiting... _About Random _Copy Item _Delete Picked Item _Help ↗ _Keyboard Shortcuts translator-credits Project-Id-Version: Random v0.6
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-02-19 15:04-0700
Last-Translator: Forever XML <foreverxml@tuta.io>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 3.0.1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: translatable="true";"
X-Poedit-SearchPath-0: data/page.codeberg.foreverxml.Random.desktop.in
X-Poedit-SearchPath-1: data/page.codeberg.foreverxml.Random.metainfo.xml.in
X-Poedit-SearchPath-2: src/window.vala
X-Poedit-SearchPath-3: src/window.ui
X-Poedit-SearchPath-4: src/shortcut.ui
 Полностью обновленный пользовательский интерфейс - главная звезда этого выпуска. Улучшенный перевод на испанский язык, новая иконка и многие другие улучшения, сделанные за последние 3 месяца. Инструмент для генерации случайных чисел или выбора случайного элемента списка. Выберите чем вам заняться, число от 1 до 100, попрыгать ли на маминой кровати, и т. д. Добавляет вид подбрасывания монет и улучшенный пользовательский интерфейс. Добавляет меню и некоторые исправления. Добавляет новый испанский перевод, справочную документацию, изменяет сочетания клавиш и делает пользовательский интерфейс более адаптивным. Добавляет несколько красивых случайных цветов. Добавляет множество новых действий и сочетаний клавиш. Монетка Элементы управления Преобразовать числа в рулетку Копировать Результат Пример: Лейла/Роуз/Кливленд/Лэмпи Первый выпуск Flathub с GTK 4. Подбросить монетку Следует GNOME HIG и использует libhandy. От: Генерировать Орёл Рандомизация ещё никогда не была такой простой Новые ярлыки, новые документы, новая жизнь. Число Преобразовать Числа в _Рулетку Крутить Генерировать, крутить или подбросить Случайный Random;Roulette;Coin;Number;Случайный;Случайная;рулетка;монета;число; Рандомизация стала проще. Убирает случайные цвета для заметной идентичности и использует последнюю версию GNOME 41, которая вышла в тот же день, что и эта. Репозиторий Рулетка Крутить колесо и удалить выпавший элемент Переключить режимы Решка Первый выпуск, добро пожаловать в мир! До: Переведена на немецкий и русский языки. Введите список, разделяя значения знаком / Версия 0.1 "Начало": Версия 0.2 "Обновление": Версия 0.3 "Пенни": Версия 0.4 "Флэт": Версия 0.5 "Гусеница": Версия 0.6 "Продвижение": Версия 0.7 "Цирк": Версия 0.8 "Синхронизация": Версия 0.9 "Глобально": Версия 1.0 "Адаптация": Версия 1.1 "Фикс-Бехинд": Версия 1.2 "Перевернутая": Версия 1.3 "Копир": Ожидающий... _О Случайный _Копировать Элемент _Удалить Выбранный Вариант _Помощь ↗ Комбинации К_лавиш Тикот <k.qovekt@gmail.com>
Форевер XML <foreverxml@tuta.io> 