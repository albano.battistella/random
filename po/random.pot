# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the randomgtk package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Random\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-02-15 20:28-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.0.1\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SearchPath-0: src/func.vala\n"
"X-Poedit-SearchPath-1: src/main.vala\n"
"X-Poedit-SearchPath-2: src/shortcut.ui\n"
"X-Poedit-SearchPath-3: src/window.ui\n"
"X-Poedit-SearchPath-4: src/window.vala\n"
"X-Poedit-SearchPath-5: data/page.codeberg.foreverxml.Random.desktop.in\n"
"X-Poedit-SearchPath-6: data/page.codeberg.foreverxml.Random.gschema.xml\n"
"X-Poedit-SearchPath-7: data/page.codeberg.foreverxml.Random.metainfo.xml.in\n"

#. Translators: This is a noun and not a verb.
#: data/page.codeberg.foreverxml.Random.desktop.in:3
#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:7 src/window.ui:4
#: src/window.ui:20 src/window.vala:108
msgid "Random"
msgstr ""

#: data/page.codeberg.foreverxml.Random.desktop.in:10
msgid "Random;Roulette;Coin;Number;"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:8
msgid "Make randomization easy"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:11
msgid ""
"A tool to pick a random number or list item. Pick what chore to do, a number "
"between 1 and 100, whether or not to jump on your mom's bed, etc."
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:33
msgid "Version 1.3 \"Copier\":"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:34
msgid "New shortcuts, new docs, new life."
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:39
msgid "Version 1.2 \"Flipped\":"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:40
msgid "A completely revamped UI is the star of this release."
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:45
msgid "Version 1.1 \"Fix-Behind\":"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:46
msgid ""
"A much better Spanish translation, a new icon, and many more improvements "
"from the past 3 months are included."
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:51
msgid "Version 1.0 \"Adapting\":"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:52
msgid ""
"Adds a new Spanish translation, help docs, revamps keyboard shortcuts, and "
"makes the UI more adaptive."
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:57
msgid "Version 0.9 \"Global\":"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:58
msgid "Translated into German and Russian."
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:63
msgid "Version 0.8 \"Sync\":"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:64
msgid ""
"Removes the random colors for a noticable identity, and uses the latest "
"GNOME 41 things, that came out on the same day as this."
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:69
msgid "Version 0.7 \"Circus\":"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:70
msgid "Adds some nice random colors."
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:75
msgid "Version 0.6 \"Advancing\":"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:76
msgid "Adds tons of new actions and keyboard shortcuts too."
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:81
msgid "Version 0.5 \"Caterpillar\":"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:82
msgid "Adds a menu and some bugfixes."
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:87
msgid "Version 0.4 \"Flat\":"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:88
msgid "First Flathub release with GTK 4."
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:93
msgid "Version 0.3 \"Penny\":"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:94
msgid "Adds a coin flip view and an improved UI."
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:99
msgid "Version 0.2 \"Makeover\":"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:100
msgid "Follows the GNOME HIG and uses libhandy."
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:105
msgid "Version 0.1 \"Beginning\":"
msgstr ""

#: data/page.codeberg.foreverxml.Random.metainfo.xml.in:106
msgid "The first release, welcome to the world!"
msgstr ""

#: src/window.ui:42
msgid "Number"
msgstr ""

#: src/window.ui:83
msgid "From:"
msgstr ""

#: src/window.ui:109
msgid "To:"
msgstr ""

#: src/window.ui:146
msgid "Generate"
msgstr ""

#: src/window.ui:167
msgid "Roulette"
msgstr ""

#: src/window.ui:177
msgid "Waiting..."
msgstr ""

#: src/window.ui:195
msgid "Example: Layla/Rose/Cleveland/Lampy"
msgstr ""

#: src/window.ui:196
msgid "Type a list, seperating values with /"
msgstr ""

#: src/window.ui:201
msgid "Pick"
msgstr ""

#: src/window.ui:219
msgid "Coin"
msgstr ""

#: src/window.ui:229 src/window.vala:93
msgid "Tails"
msgstr ""

#: src/window.ui:237
msgid "Flip the coin"
msgstr ""

#. Translators: Remember to add a mnemonic and use header case here.
#: src/window.ui:270
msgid "Number to _Roulette"
msgstr ""

#. Translators: Remember to add a mnemonic and use header case here.
#: src/window.ui:275
msgid "_Delete Picked Item"
msgstr ""

#. Translators: Remember to add a mnemonic and use header case here.
#: src/window.ui:280
msgid "_Copy Item"
msgstr ""

#. Translators: Remember to add a mnemonic and use header case here.
#: src/window.ui:287
msgid "_Keyboard Shortcuts"
msgstr ""

#. Translators: Remember to add a mnemonic and use header case here.
#: src/window.ui:292
msgid "_Help ↗"
msgstr ""

#. Translators: Remember to add a mnemonic and use header case here.
#: src/window.ui:297
msgid "_About Random"
msgstr ""

#. coinflip
#: src/window.vala:93
msgid "Heads"
msgstr ""

#. Translators: add your names and emails to this table, one per line as shown
#: src/window.vala:104
msgid "translator-credits"
msgstr ""

#: src/window.vala:111
msgid "Randomizing made easy."
msgstr ""

#: src/window.vala:118
msgid "Repository"
msgstr ""

#: src/shortcut.ui:9
msgid "Controls"
msgstr ""

#: src/shortcut.ui:13
msgid "Convert from Number to Roulette"
msgstr ""

#: src/shortcut.ui:19
msgid "Spin Roulette and delete selected"
msgstr ""

#: src/shortcut.ui:25
msgid "Pick, spin, or flip"
msgstr ""

#: src/shortcut.ui:31
msgid "Switch views"
msgstr ""

#: src/shortcut.ui:37
msgid "Copy result"
msgstr ""
